// -*- mode:c++; indent-tabs-mode:nil; -*-
#include <cox.h>

Timer ledTimer;
Timer printTimer;

static void ledOffTask(void *args);

static void ledOnTask(void *args) {
  ledTimer.onFired(ledOffTask, NULL);
  ledTimer.startOneShot(500);
  digitalWrite(19, HIGH);
  digitalWrite(20, LOW);
}

static void ledOffTask(void *args) {
  ledTimer.onFired(ledOnTask, NULL);
  ledTimer.startOneShot(500);
  digitalWrite(19, LOW);
  digitalWrite(20, HIGH);
}

static const char *weekday[] = { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };
static void printTask(void *args) {
  Serial.write("0123456789\r\n");
  printf("[%lu usec] Timer works!\n", micros());

  struct tm t;
  System.getDateTime(t);
  struct timeval now;
  gettimeofday(&now, NULL);
  printf(
    "Now: %u-%u-%u %s %02u:%02u:%02u (%lu.%06ld)\n",
    t.tm_year + 1900,
    t.tm_mon + 1,
    t.tm_mday,
    weekday[t.tm_wday],
    t.tm_hour,
    t.tm_min,
    t.tm_sec,
    (uint32_t) now.tv_sec,
    now.tv_usec
  );

  int32_t vRef, vBat;
  vRef = System.getAnalogReferenceVoltage();
  vBat = System.getBatteryVoltage();
  printf("V_REF+: %d mV, V_BAT:%d mV\n", vRef, vBat);

  Serial2.write("0123456789\r\n");
}

static void eventDateTimeAlarm() {
  struct tm t;
  System.getDateTime(t);
  printf("* Alarm! Now: %u-%u-%u %s %02u:%02u:%02u\n", t.tm_year, t.tm_mon, t.tm_mday, weekday[t.tm_wday], t.tm_hour, t.tm_min, t.tm_sec);
}

static void buttonPressed() {
  printf("[%lu usec] Button works!\n", micros());
}

static void eventSerialRx(SerialPort &p) {
  while (p.available() > 0) {
    char c = p.read();
    if (c == 'q') {
      reboot();
    } else {
      printf("%02X ", c);
      p.write(c);
    }
  }
}

void setup() {
  pinMode(19, OUTPUT);
  digitalWrite(19, HIGH);
  
  Serial.begin(115200);
  Serial.onReceive(eventSerialRx);
  Serial.listen();

  Serial2.begin(115200);
  // Serial2.onReceive(eventSerialRx);
  // Serial2.listen();

  printf("\n*** [RAK3172] Basic Functions ***\n");
  printf("- Last reset reason: 0x%lx\n", System.getResetReason());

  printTimer.onFired(printTask, NULL);
  printTimer.startPeriodic(1000);

}
