#include <cox.h>
#include <RS485.hpp>

RS485 rs485(Serial, 29, 30);
Timer timerEoL;

void setup() {
  Serial2.begin(115200);

  printf("\n*** [RAK3172] RS485 Listener ***\n");
  printf("- Last reset reason: 0x%lx\n", System.getResetReason());

  rs485.begin(115200);
  rs485.onReceive([](void *) {
    while (rs485.available() > 0) {
      char c = rs485.read();
      printf("%02X ", c);
    }
    timerEoL.startOneShot(500);
  }, nullptr);
  rs485.receive();

  timerEoL.onFired([](void *) {
    printf("\n");
  });
}
