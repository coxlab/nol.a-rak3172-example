// -*- mode:c++; indent-tabs-mode:nil; -*-
#include <cox.h>
#include <dev/SHTSensor.hpp>

Timer printTimer;

SHTSensor sht(SHTSensor::SHT3X);

static void printTask(void *args) {
  if (sht.readSample()) {
    printf("[SHT3x] T: %f, RH: %f\n", sht.getTemperature(), sht.getHumidity());
  } else {
    printf("[SHT3x] read fail\n");
  }
}

void setup() {
  Serial2.begin(115200);
  printf("\n*** [RAK3172] Basic Functions ***\n");
  printf("- Last reset reason: 0x%lx\n", System.getResetReason());

  printTimer.onFired(printTask);
  printTimer.startPeriodic(1000);

  pinMode(20, OUTPUT);
  digitalWrite(20, HIGH);

  Wire.begin();
  sht.init(Wire);
}
